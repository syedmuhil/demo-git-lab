import { defineSupportCode } from 'cucumber';
import { browser } from 'protractor';

import { CommonTasks, HomePage, LoginPage } from '../shared';

defineSupportCode(({ Given, When, Then }) => {

    const chai = require('chai').use(require('chai-as-promised'));
    const expect = chai.expect;
    const lPage = new LoginPage();
    const hPage = new HomePage();
    
    Given('User is on Home Page', {timeout: 150000}, async () => {
        await CommonTasks.wait(5000);
        await browser.get('', 120000);
    });

    Given('Logs in as an existing Customer', {timeout: 150000}, async () => {
        await CommonTasks.wait(3000);
        await lPage.customerLoginButton.click();
        await lPage.customerNameList.$('[value="1"]').click();
        await CommonTasks.wait(3000);
        return await lPage.loginButton.click();
    });

    When('He withdraws an amount less than the overdraft limit', {timeout: 60000}, async () => {
        await CommonTasks.wait(3000);
        await hPage.withdrawalButton.click();
        await CommonTasks.wait(3000);
        return await hPage.amountTextBox.sendKeys('400');
    });

    When('He Deposits a valid amount of Money', {timeout: 60000}, async () => {
        await CommonTasks.wait(3000);
        await hPage.depositButton.click();
        await CommonTasks.wait(3000);
        await hPage.depositAmountTextBox.sendKeys('400');
        return await hPage.completeDepositButton.click();
    });    

    Then('Transaction is Successful', async () => {
        await CommonTasks.wait(2000);
        await hPage.withdrawButton.click();
        await expect(hPage.transactionSuccessfulText.isPresent()).to.be.eventually.false;
        return await CommonTasks.wait(1000);
    });

    Then('Deposit is Successful', async () => {
        await CommonTasks.wait(2000);
        await expect(hPage.depositSuccessfulText.isPresent()).to.be.eventually.true;
        return await CommonTasks.wait(1000);
    });

});
